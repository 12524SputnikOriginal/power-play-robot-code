package org.firstinspires.ftc.teamcode.modules.PID;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class Parameters implements Cloneable {
    public double kP;
    public double kI;
    public double kD;
    public double valueTolerance;
    public double velocityTolerance;
    public double maxSettingTime;
    public double integralDelta;
    public LinearOpMode opMode;
    public Consumer<Double> setControlAction;
    public Supplier<Double> actualValue;
    public Supplier<Double> velocityValue;
    public boolean showDashmetry;

    public Parameters(double kP, double kI, double kD,
                      double valueTolerance, double velocityTolerance,
                      double maxSettingTime, double integralDelta,
                      Consumer<Double> setControlAction,
                      Supplier<Double> actualValue, Supplier<Double> velocityValue,
                      boolean showDashmetry) {
    }
}
