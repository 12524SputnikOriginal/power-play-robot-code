package org.firstinspires.ftc.teamcode.OpModes;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.modules.Drivetrain;

@Autonomous(group = "Autonomous", name = "DrivetrainTestProgramm")
public class DrivetrainTestProgramm extends LinearOpMode {
    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        waitForStart();

        dt.driveFront(0.5, 5000);
        sleep(100);
        dt.driveLeft(0.5, 5000);
        sleep(100);
        dt.driveRight(0.5, 5000);
        sleep(100);
        dt.driveBack(0.5, 5000);
        sleep(100);
        dt.drive(0, 0, 360);
        sleep(100);
        dt.drive(0, 0, -360);
        sleep(100);
    }

}
