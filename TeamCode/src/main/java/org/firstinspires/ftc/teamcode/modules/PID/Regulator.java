package org.firstinspires.ftc.teamcode.modules.PID;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.modules.MetryHandler;

public class Regulator {

    private final Parameters parameters;
    private final MetryHandler mh;

    public Regulator(LinearOpMode _opMode, Parameters _parameters) {
        parameters = _parameters;
        mh = new MetryHandler(_opMode, "D");
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    /**
     * Поддержание величины из PID-коэффициентов и системы обратной свзяи
     * setValue - заданное значение
     * actualValue - реальная величина
     * kP, kI, kD - коэффициенты PID
     * d0 - ошибка
     * d1 - ошибка спустя время обновления показаний датчиков
     * dDerivative - производная ошибки
     * dIntegral - интеграл ошибки
     * u - управляющее воздействие
     * calcTime - время обновления показаний, подсчёта интеграла, производной
     * setTime - время установки на курс
     * deltaTime - время обновления показаний
     * @param course - курс в градусах, заданное значение
     */
    public void set(double setValue) {
        double d0 = setValue - parameters.actualValue.get();
        double d1 = setValue - parameters.actualValue.get();

        ElapsedTime calcTime = new ElapsedTime();
        ElapsedTime setTime = new ElapsedTime();

        double dIntegral = 0;
        double dDerivative = (d1 - d0) / (calcTime.nanoseconds() * 10e-9);
        double u = parameters.kP * d1 + parameters.kI * dIntegral + parameters.kD * dDerivative;

        setTime.reset();
        calcTime.reset();

        while (parameters.opMode.opModeIsActive() && (setTime.seconds() < parameters.maxSettingTime) &&
                ((Math.abs(d0) > parameters.valueTolerance) ||
                        (Math.abs(parameters.velocityValue.get()) > parameters.velocityTolerance))) {

            d1 = setValue - parameters.actualValue.get();
            dDerivative = (d1 - d0) / (calcTime.nanoseconds() * 10e-9);
            if (d1 < parameters.integralDelta) {
                dIntegral += d1 * calcTime.nanoseconds() * 10e-9;
            }

            u = parameters.kP * d1 + parameters.kI * dIntegral + parameters.kD * dDerivative;
            parameters.setControlAction.accept(u);

            d0 = setValue - parameters.actualValue.get();


            if (parameters.showDashmetry) {
                mh.addData("SettingTime", setTime.seconds());
                mh.addData("deltaTime", calcTime.nanoseconds() * 10e-9);
                mh.addLine("");
                mh.addData("SetValue", setValue);
                mh.addData("Value", parameters.actualValue.get());
                mh.addData("D0", d0);
                mh.addData("D1", d1);
                mh.addLine("");
                mh.addData("dDerivative", dDerivative);
                mh.addData("dIntegral", dIntegral);
                mh.addLine("");
                mh.addData("U", u);
                mh.addData("uP", parameters.kP * d1);
                mh.addData("uI", parameters.kI * dIntegral);
                mh.addData("uD", parameters.kD * dDerivative);
                mh.update();
            }

            calcTime.reset();
        }
        parameters.setControlAction.accept(0.0);
        setTime.reset();
    }

}
