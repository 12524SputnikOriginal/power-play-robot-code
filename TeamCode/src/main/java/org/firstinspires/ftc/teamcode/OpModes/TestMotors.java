package org.firstinspires.ftc.teamcode.OpModes;

import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.modules.Drivetrain;

@Config
@TeleOp(group = "Test", name = "Motors")
public class TestMotors extends LinearOpMode {
    private final boolean rotationMode = true;
    private final float angle = 0;

    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);

        waitForStart();
        while (opModeIsActive()) {
            dt.checkMotors(gamepad1.a, gamepad1.b, gamepad1.x, gamepad1.y);
            telemetry.addLine("a - leftFront, b - rightFront, x - leftBack, y - rightBack");
            telemetry.addData("a", gamepad1.a);
            telemetry.addData("b", gamepad1.b);
            telemetry.addData("x", gamepad1.x);
            telemetry.addData("y", gamepad1.y);
            telemetry.update();
        }
    }
}
