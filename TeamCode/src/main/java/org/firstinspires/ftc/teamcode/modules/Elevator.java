package org.firstinspires.ftc.teamcode.modules;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.Telemetry;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


@Config
public class Elevator {
    public static final int COIL_DIAMETER = 45;
    public static double UpPowerMotorElevator = -0.3;
    public static double DownPowerMotorElevator = 0.3;
    public static double K_UP_POWER = 1;
    public static double K_DOWN_POWER = 0.4;
    public static double K_DOWN_DOUBLE_POWER = 0.6;

    public static int TickElevator;
    public final int ENCODER_NUMBER = 28;
    public final int NUMBER_CARTRIDGES = 15;
    public final int SUBORDINATE_NUMBER = ENCODER_NUMBER * NUMBER_CARTRIDGES;
    private final DcMotor leftDrive;
    private final DcMotor rightDrive;
    private final LinearOpMode opMode;
    private final boolean launch = false;
    private final boolean isUp = false;

    public static double kP = 0.03;
    public static double kI = 0;
    public static double kD = -0.001;
    public static double VELOCITY_TOLERANCE = 1;
    public static double HEIGHT_TOLERANCE = 0.5;
    public static double MAX_TIME = 5;
    public double set_value;
    /* взаимодействие с основным потоком, когда оно понадобится, то будет true */
    private final Executor executor;
    ElevatorController controller = new ElevatorController();
    Telemetry dm = FtcDashboard.getInstance().getTelemetry();

    public Elevator(LinearOpMode _opMode) {
        opMode = _opMode;
        executor = Executors.newSingleThreadExecutor();
        HardwareMap hm = opMode.hardwareMap;
        leftDrive = hm.dcMotor.get("ld");
        rightDrive = hm.dcMotor.get("rd");
        controller.start();

        rightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            rightDrive.setDirection(DcMotorSimple.Direction.REVERSE);
        leftDrive.setDirection(DcMotorSimple.Direction.REVERSE);
    }

    public void setTargetPosition(double goal) {
        set_value = goal;
    }

    public boolean isContollerEnabled() {
        return controller.isOn;
    }

    public void enableController() {
        controller.isOn = true;
    }

    public void disableController() {
        controller.isOn = false;
    }

    public void interruptController(){
        controller.interrupt();
    }

    public void drive(double y) {
        if (y >= 0) {
            leftDrive.setPower(y * K_UP_POWER);
            rightDrive.setPower(y * K_UP_POWER);

        } else {
            if (getHeight() > 25) {
                leftDrive.setPower(y * K_DOWN_POWER);
            } else {
                leftDrive.setPower(0);
            }
            if (getHeight() > 25) {
                rightDrive.setPower(y * K_DOWN_POWER);
            } else {
                rightDrive.setPower(0);
            }

        }
        dm.addData("LeftEncoderPosition", leftDrive.getCurrentPosition());
        dm.addData("RightEncoderPosition", rightDrive.getCurrentPosition());
        dm.addData("power", y);
        dm.addData("Height", getHeight());
        dm.update();


    }

    //<<<<<<< HEAD
//    //Вверх по энкодерам
//    public void up() {
//=======
    public void up() {
        int position1 = leftDrive.getCurrentPosition();
        int position2 = rightDrive.getCurrentPosition();
        drive(UpPowerMotorElevator);
        Telemetry telemetry = FtcDashboard.getInstance().getTelemetry();
        while (!(leftDrive.getPower() == 0 && rightDrive.getPower() == 0)) {
            int leftDifference = Math.abs(position1 - leftDrive.getCurrentPosition());
            int rightDifference = Math.abs(position2 - rightDrive.getCurrentPosition());
            if (leftDifference >= TickElevator) {
                leftDrive.setPower(0);
            }
            if (rightDifference >= TickElevator) {
                rightDrive.setPower(0);
            }
            telemetry.addData("Левый передний:", leftDifference);
            telemetry.addData("Правый передний:", rightDifference);
            telemetry.update();
        }
    }

    //Вниз по энкодерам
    public void down() {
        int position1 = leftDrive.getCurrentPosition();
        int position2 = rightDrive.getCurrentPosition();
        drive(DownPowerMotorElevator);
        Telemetry telemetry = FtcDashboard.getInstance().getTelemetry();
        while (!(leftDrive.getPower() == 0 && rightDrive.getPower() == 0)) {
            int leftDifference = Math.abs(position1 - leftDrive.getCurrentPosition());
            int rightDifference = Math.abs(position2 - rightDrive.getCurrentPosition());
            if (leftDifference >= TickElevator) {
                leftDrive.setPower(0);
            }
            if (rightDifference >= TickElevator) {
                rightDrive.setPower(0);
            }

        }
    }

    public void getPositionUp(double correct_Position) {
        while (opMode.opModeIsActive()) {
            if (correct_Position > rightDrive.getCurrentPosition()) {
                drive(0.5);
            } else {
                drive(-0.5);
            }
        }
    }

    public void getPositionDown(double correct_Position) {
        while (opMode.opModeIsActive()) {
            if (correct_Position > rightDrive.getCurrentPosition()) {
                drive(0.5);
            } else {
                drive(-0.5);
            }
        }


    }

    public double getHeight() {
        double difference = (getHeightRight() + getHeightLeft()) / 2;
        return difference;
    }

    private double getHeightRight() {
        double heightR;
        double turnoversR = (double) rightDrive.getCurrentPosition() / SUBORDINATE_NUMBER;
        heightR = turnoversR * COIL_DIAMETER * Math.PI;
        return heightR;
    }

    private double getHeightLeft() {
        double heightL;
        double turnoversL = (double) leftDrive.getCurrentPosition() / SUBORDINATE_NUMBER;
        heightL = turnoversL * COIL_DIAMETER * Math.PI;
        return heightL;
    }


    class ElevatorController extends Thread {
        boolean isOn = true;

        public void run() {
            double d0L = 0;
            double d0R = 0;
            double d1L;
            double d1R;

            ElapsedTime calcTime = new ElapsedTime();
            ElapsedTime setTime = new ElapsedTime();
            ElapsedTime deltaTime = new ElapsedTime();

            double dIntegralL = 0;
            double dIntegralR = 0;
            double dDerivativeL;
            double dDerivativeR;
            double uR;
            double uL;

            double deltatime;


            setTime.reset();
            calcTime.reset();
            deltaTime.reset();
            while (!isInterrupted()) {
                while (isContollerEnabled()) {
                    deltatime = deltaTime.seconds();
                    deltaTime.reset();
                    d1L = set_value - getHeightLeft();
                    d1R = set_value - getHeightRight();
                    dDerivativeL = (d1L - d0L) / (calcTime.nanoseconds() * 10e-9);
                    dIntegralL = d1L * calcTime.nanoseconds() * 10e-9;
                    dDerivativeR = (d1R - d0R) / (calcTime.nanoseconds() * 10e-9);
                    dIntegralR = d1R * calcTime.nanoseconds() * 10e-9;

                    uL = kP * d1L + kI * dIntegralL + kD * dDerivativeL;
                    uR = kP * d1R + kI * dIntegralR + kD * dDerivativeR;
                    if (set_value == 0 && Math.abs(getHeight()) < 5) {
                        leftDrive.setPower(0);
                        rightDrive.setPower(0);
                    } else {
                        leftDrive.setPower(uL);
                        rightDrive.setPower(uR);
                    }
                    if (isInterrupted()) {
                        leftDrive.setPower(0);
                        rightDrive.setPower(0);
                        break;
                    }
                    d0L = set_value - getHeightLeft();
                    d0R = set_value - getHeightRight();

                    calcTime.reset();

                    dm.addData("Высота", set_value);
                    dm.addData("ActualValue", getHeight());
                    dm.addData("d0L", d0L);
                    dm.addData("d1L", d1L);
                    dm.addData("dDerivativeL", dDerivativeL);
                    dm.addData("dIntegralL", dIntegralL);
                    dm.addData("uL", uL);
                    dm.addData("uPL", kP * d1L);
                    dm.addData("uIL", kI * dIntegralL);
                    dm.addData("uDL", kD * dDerivativeL);
                    dm.addLine("");
                    dm.addData("d0R", d0R);
                    dm.addData("d1R", d1R);
                    dm.addData("dDerivativeR", dDerivativeR);
                    dm.addData("dIntegralR", dIntegralR);
                    dm.addData("UL", uL);
                    dm.addData("uPR", kP * d1R);
                    dm.addData("uIR", kI * dIntegralR);
                    dm.addData("uDR", kD * dDerivativeR);
                    dm.update();
                }

            }
        }
    }
}
