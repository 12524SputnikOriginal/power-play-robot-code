package org.firstinspires.ftc.teamcode.examples;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.robotcore.external.Telemetry;


@TeleOp
@Config
public class DashboardSample extends LinearOpMode {
    public static int STEP = 1;
    int VALUE = 0;

    @Override
    public void runOpMode() {
        FtcDashboard dashboard = FtcDashboard.getInstance();
        Telemetry dashboardTelemetry = dashboard.getTelemetry();

        waitForStart();
        while (opModeIsActive()) {
            dashboardTelemetry.addData("STEPS:", STEP);
            dashboardTelemetry.addData("VALUE:", VALUE);
            dashboardTelemetry.update();
            VALUE += STEP;
            sleep(2000);
        }
    }
}
