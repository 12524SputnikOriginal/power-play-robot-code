package org.firstinspires.ftc.teamcode.OpModes;

import com.acmerobotics.dashboard.FtcDashboard;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.teamcode.modules.Claw;
import org.firstinspires.ftc.teamcode.modules.Drivetrain;
import org.firstinspires.ftc.teamcode.modules.ImuSensor;

@TeleOp(name = "TeleOpTest")
public class TeleOpTest extends LinearOpMode {
    private boolean xPrewState = false; //отвечает за предыдущее состояние кнопки х

    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        ImuSensor imu = new ImuSensor(this);
        Claw cl = new Claw(this);
        Telemetry dm = FtcDashboard.getInstance().getTelemetry();
        //MetryHandler dm = new MetryHandler(this, "D");
        waitForStart();

        while (opModeIsActive()) {
//            telemetry.addData("xPrewState: ", xPrewState);
//            telemetry.addData("isOpened?: ", cl.isOpened());
//            telemetry.addData("y_left_stick", gamepad1.left_stick_y);
            dm.addData("Angle", imu.getAngles());
            dm.update();
            //управление колесной базой
            //левый стик отвечает за езду
            //х отвечает за закрытие/открытие клешней
            if (gamepad1.dpad_up) {
                dt.driftLeftFront(gamepad1.left_stick_x, gamepad1.left_stick_y, gamepad1.right_trigger - gamepad1.left_trigger);
            } else if (gamepad1.dpad_left) {
                dt.driftRightFront(gamepad1.left_stick_x, gamepad1.left_stick_y, gamepad1.right_trigger - gamepad1.left_trigger);
            } else if (gamepad1.dpad_down) {
                dt.driftRightBack(gamepad1.left_stick_x, gamepad1.left_stick_y, gamepad1.right_trigger - gamepad1.left_trigger);
            } else if (gamepad1.dpad_right) {
                dt.driftLeftBack(gamepad1.left_stick_x, gamepad1.left_stick_y, gamepad1.right_trigger - gamepad1.left_trigger);
            } else {
                dt.drive(gamepad1.left_stick_x, gamepad1.left_stick_y, gamepad1.right_trigger - gamepad1.left_trigger);
            }
            //управление клешней
            //х - открыть/закрыть
            if (gamepad1.x && !xPrewState) {
                if (cl.isOpened()) {
                    cl.close();
                } else {
                    cl.open();
                }
            }
            xPrewState = gamepad1.x;
            sleep(1);
            if (gamepad1.a) {
                cl.middle();
            }
        }

    }
}
