package org.firstinspires.ftc.teamcode.OpModes;

import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.modules.Drivetrain;
import org.firstinspires.ftc.teamcode.modules.ImuSensor;

@TeleOp(group = "Test", name = "ImuPID")
@Config
public class TestImuPID extends LinearOpMode {
    public static float setCourse = 45;
    public static boolean a_btn_state = true;
    public static boolean b_btn_state = true;
    public static boolean x_btn_state = true;
    public static boolean y_btn_state = true;


    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        ImuSensor imu = new ImuSensor(this);
//        Dashmetry dm = new Dashmetry();
//        dm.addLine("A - установка на курс 0");
//        dm.addLine("X - вращение на -45 градусов");
//        dm.addLine("Y - вращение на +45 градусов");


        waitForStart();
        while (opModeIsActive()) {
            if (gamepad1.a && a_btn_state) {
                a_btn_state = false;
                dt.coursePID(0);

            } else if (gamepad1.x && b_btn_state) {
                for (int i = 1; i < 8; ++i) {
                    dt.rotatePID(-45);
                    sleep(1000);
                }
            } else if (gamepad1.y && b_btn_state) {
                for (int i = 1; i < 8; ++i) {
                    dt.rotatePID(45);
                    sleep(1000);
                }
            } else {
                a_btn_state = true;
                b_btn_state = true;
                x_btn_state = true;
                y_btn_state = true;

                dt.drive(gamepad1.left_stick_x, gamepad1.left_stick_y, gamepad1.right_trigger - gamepad1.left_trigger);
//                dm.addData("ImuSensor", imu.getAngles());
//                dm.update();
            }
        }
    }
}
