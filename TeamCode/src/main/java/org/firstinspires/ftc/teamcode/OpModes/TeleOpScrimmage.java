package org.firstinspires.ftc.teamcode.OpModes;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.modules.Claw;
import org.firstinspires.ftc.teamcode.modules.Drivetrain;
import org.firstinspires.ftc.teamcode.modules.Elevator;
@Disabled
@TeleOp(name = "TeleOpScrimmage")
public class TeleOpScrimmage extends LinearOpMode {
    private boolean xPrewState = false; //отвечает за предыдущее состояние кнопки х


    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Claw cl = new Claw(this);
        Elevator el = new Elevator(this);
        waitForStart();

        while (opModeIsActive()) {
            telemetry.addData("xPrewState: ", xPrewState);
            telemetry.addData("isOpened?: ", cl.isOpened());
            telemetry.addData("y_left_stick", gamepad1.left_stick_y);
            telemetry.update();
            //управление колесной базой
            //левый стик отвечает за езду
            //х отвечает за закрытие/открытие клешней
            dt.drive(gamepad1.left_stick_x, gamepad1.left_stick_y, gamepad1.right_trigger - gamepad1.left_trigger);
            //управление клешней
            //х - открыть/закрыть
            if (gamepad2.x && !xPrewState) {
                if (cl.isOpened()) {
                    cl.close();
                } else {
                    cl.open();
                }
            }
            xPrewState = gamepad2.x;
            sleep(1);
            if (gamepad1.a) {
                cl.middle();
            }
            if (gamepad2.dpad_down) {
                cl.close();
                el.drive(-1);
            } else {
                if (gamepad2.dpad_up) {
                    el.drive(1);
                } else {
                    el.drive(0);
                }
            }
        }
        el.interruptController();
    }
}
