package org.firstinspires.ftc.teamcode.OpModes;

import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.modules.Claw;
import org.firstinspires.ftc.teamcode.modules.Drivetrain;
import org.firstinspires.ftc.teamcode.modules.Elevator;

@Config
@TeleOp(name = "TeleOpSecondScrimmage")
public class TeleOpSecondScrimmage extends LinearOpMode {
    public static final int TARGET_HIGH = 950;
    public static final int TARGET_MIDDLE = 685;
    public static final int TARGET_LOW = 415;
    public static int TARGET_DELTA = 50;
    public static int TARGET_STACK = TARGET_LOW;
    public static final int TARGET_GAP = 150;
    public final int TARGET_DOWN = 2;
    public int targetsecond = 0;
    private boolean xPrewState = false;//оотвечает за предыдущее состоянии кнопки х
    private boolean yPrewState = false;//отвечает за предыдущее состояние кнопки y

    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Claw cl = new Claw(this);
        Elevator El = new Elevator(this);
        waitForStart();

        while (opModeIsActive()) {
            telemetry.addData("xPrewState: ", xPrewState);
            telemetry.addData("yPrewState: ", yPrewState);
            telemetry.addData("isOpened?: ", cl.isOpened());
            telemetry.addData("y_left_stick", gamepad1.left_stick_y);
            //управление колесной базой
            //левый стик отвечает за езду
            //х отвечает за закрытие/открытие клешней
            dt.drive(gamepad1.right_stick_x, gamepad1.left_stick_y, gamepad1.right_trigger - gamepad1.left_trigger);
            //управление клешней
            //х - открыть/закрыть
            if (gamepad2.x && !xPrewState) {
                if (cl.isOpened()) {
                    cl.close();
                } else {
                    cl.open();
                }
            }
            xPrewState = gamepad2.x;
            sleep(1);
            if (gamepad2.y && !yPrewState) {
                if (El.isContollerEnabled()) {
                    El.disableController();
                } else {
                    El.enableController();
                }
            }

            yPrewState = gamepad2.y;
            if (!El.isContollerEnabled()) {
                if (Math.abs(gamepad2.right_stick_y) > 0.05) {
                    cl.close();

                    El.drive(-gamepad2.right_stick_y);
                } else {
                    El.drive(0);
                }


            } else {
                if (gamepad2.dpad_up) {
                    //для тестов менять targetsecond+10
                    El.setTargetPosition(TARGET_MIDDLE);
                    sleep(20);
                }
                if (gamepad2.dpad_down) {
                    //для тестов менять targetsound-10
                    El.setTargetPosition(TARGET_DOWN);
                    sleep(20);
                }
                if (gamepad2.dpad_left) {
                    El.setTargetPosition(TARGET_LOW);
                    sleep(20);
                }
                if (gamepad2.dpad_right) {
                    El.setTargetPosition(TARGET_HIGH);
                    sleep(20);
                }
                if (gamepad2.a) {
                    El.setTargetPosition(TARGET_GAP);
                    sleep(20);
                }
                if (gamepad2.right_bumper) {
                    if (TARGET_STACK > TARGET_DOWN + TARGET_DELTA) {
                        TARGET_STACK -= TARGET_DELTA;
                    }
                    El.setTargetPosition(TARGET_STACK);
                    sleep(20);
                }
                if (gamepad2.left_bumper) {
                    if (TARGET_STACK < TARGET_LOW) {
                        TARGET_STACK += TARGET_DELTA;
                    }
                    El.setTargetPosition(TARGET_STACK);
                    sleep(20);
                }
            }
        }
        El.disableController();
        El.interruptController();
        telemetry.addData("Позиция", El.getHeight());
        telemetry.addData("Контроллер", El.isContollerEnabled());
        telemetry.addData("Цель", targetsecond);
        telemetry.update();
    }
}
