package org.firstinspires.ftc.teamcode.examples;


import android.os.Build;

import androidx.annotation.RequiresApi;

import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.modules.Drivetrain;
import org.firstinspires.ftc.teamcode.modules.ImuSensor;
import org.firstinspires.ftc.teamcode.modules.PID.Parameters;
import org.firstinspires.ftc.teamcode.modules.PID.Regulator;

@Autonomous(group = "Example", name = "ExamplePIDModule")
@Config
public class PIDmoduleExample extends LinearOpMode {
    public static double kP = 0.0225;
    public static double kI = 0.0225;
    public static double kD = 0.0225;
    public static double valTol = 0.5;
    public static double velTol = 1;
    public static double maxSetTime = 5;
    public static double intDelta = 8;

    Drivetrain dt = new Drivetrain(this);
    ImuSensor imu = new ImuSensor(this);
    Parameters dtPar = new Parameters(kP, kI, kD, valTol, velTol, maxSetTime, intDelta,
            (p) -> dt.drive(0, 0, p), () -> imu.getAngles(), () -> imu.getAngularVelocity(), true);
    Regulator dtReg = new Regulator(this, dtPar);

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void runOpMode() {
        while (opModeIsActive()) {
            dtReg.set(0);
            dtReg.set(90);
            dtReg.set(180);
            dtReg.set(270);
        }
    }
}
