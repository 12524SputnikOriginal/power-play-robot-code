package org.firstinspires.ftc.teamcode.modules.recognition;


import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.openftc.easyopencv.OpenCvCamera;
import org.openftc.easyopencv.OpenCvCameraFactory;
import org.openftc.easyopencv.OpenCvCameraRotation;

public class Recognition {
    public static final int FIRST_ZONE_CODE = 30;
    public static final int SECOND_ZONE_CODE = 32;
    public static final int THIRD_ZONE_CODE = 28;
    OpenCvCamera camera;
    AprilTagDetectionPipeline aprilTagDetectionPipeline;
    double fx = 578.272; //нужно откалибровать для своей камеры
    double fy = 578.272; //нужно откалибровать для своей камеры
    double cx = 402.145; //нужно откалибровать для своей камеры
    double cy = 221.506; //нужно откалибровать для своей камеры
    double tagSize = 0.166;

    public Recognition(LinearOpMode _opMode) {
        HardwareMap hm = _opMode.hardwareMap;
        int cameraMonitorViewId = hm.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hm.appContext.getPackageName());
        camera = OpenCvCameraFactory.getInstance().createWebcam(hm.get(WebcamName.class, "Webcam 1"), cameraMonitorViewId);
        aprilTagDetectionPipeline = new AprilTagDetectionPipeline(tagSize, fx, fy, cx, cy);

        camera.setPipeline(aprilTagDetectionPipeline);
        camera.openCameraDeviceAsync(new OpenCvCamera.AsyncCameraOpenListener() {
            @Override
            public void onOpened() {
                camera.startStreaming(800, 448, OpenCvCameraRotation.UPRIGHT);
            }

            @Override
            public void onError(int errorCode) {

            }
        });
        _opMode.telemetry.setMsTransmissionInterval(5);
    }

    public int getId() {
        if (aprilTagDetectionPipeline.getLatestDetections().size() != 0)
            return aprilTagDetectionPipeline.getLatestDetections().get(0).id;
        else
            return -1;
    }
}
