package org.firstinspires.ftc.teamcode.OpModes;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.modules.Claw;

@TeleOp(group = "Test", name = "Claw")
public class TestClaw extends LinearOpMode {
    @Override
    public void runOpMode() {
        Claw cl = new Claw(this);
        String state = "None";
        cl.open();
        waitForStart();
        cl.close();
        while (opModeIsActive()) {
            if (gamepad1.a) {
                cl.close();
                state = "Closed";
            } else if (gamepad1.b) {
                cl.middle();
                state = "Middle";
            } else if (gamepad1.x) {
                cl.open();
                state = "Opened";
            }
            telemetry.addData("State", state);
            sleep(500);
        }
    }
}

