package org.firstinspires.ftc.teamcode.modules;

import com.acmerobotics.dashboard.FtcDashboard;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.robotcore.external.Telemetry;

public class MetryHandler {
    Telemetry tm;
    Telemetry dm;
    String mode;

    public MetryHandler(LinearOpMode opMode, String _mode) {
        mode = _mode;
        if (mode == "DT") {
            tm = opMode.telemetry;
            dm = FtcDashboard.getInstance().getTelemetry();
        } else if (mode == "T") {
            tm = opMode.telemetry;
        } else if (mode == "D") {
            dm = FtcDashboard.getInstance().getTelemetry();
        }
    }

    public void addData(String caption, Number value) {
        if (mode == "DT") {
            tm.addData(caption, value);
            dm.addData(caption, value);
        } else if (mode == "T") {
            tm.addData(caption, value);
        } else if (mode == "D") {
            dm.addData(caption, value);
        }
    }

    public void addLine(String line) {
        if (mode == "DT") {
            tm.addLine(line);
            dm.addLine(line);
        } else if (mode == "T") {
            tm.addLine(line);
        } else if (mode == "D") {
            dm.addLine(line);
        }
    }

    public void update() {
        if (mode == "DT") {
            tm.update();
            dm.update();
        } else if (mode == "T") {
            tm.update();
        } else if (mode == "D") {
            dm.update();
        }
    }
}
