package org.firstinspires.ftc.teamcode.OpModes;

import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.modules.Drivetrain;
import org.firstinspires.ftc.teamcode.modules.ImuSensor;

@Config
@TeleOp(group = "Test", name = "Imu")
public class TestImu extends LinearOpMode {
    private final boolean rotationMode = true;
    private float angle = 0;

    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        ImuSensor imu = new ImuSensor(this);

        waitForStart();
        while (opModeIsActive()) {
            if (gamepad1.a) {
                angle += 1;
            } else if (gamepad1.b) {
                angle -= 1;
            }
            if (gamepad1.x) {
                dt.rotate(angle);
            }
            if (gamepad1.y) {
                dt.course(angle);
            }
            telemetry.addLine("x = rotate; y = course");
            telemetry.addData("Angle:", imu.getAngles());
            telemetry.addData("_angle:", angle);
            telemetry.update();
        }
    }
}
