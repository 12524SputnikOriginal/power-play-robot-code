package org.firstinspires.ftc.teamcode.OpModes;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.modules.Claw;
import org.firstinspires.ftc.teamcode.modules.Drivetrain;

@Autonomous(group = "Autonomous", name = "AutonomousSecondScrimmage")
public class AutonomousSecondScrimmage extends LinearOpMode {
    public static double DRIVE_POWER = -0.3;
    public static double TICK = 1120;

    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Claw cl = new Claw(this);
        cl.close();
        waitForStart();
        dt.driveFront(-0.3, 1550);
        dt.driveLeft(0.3, 1000);


        telemetry.addData("DRIVE POWER:", DRIVE_POWER);
        telemetry.addData("TICK: ", TICK);
        telemetry.update();
        dt.encoder(TICK, DRIVE_POWER);

    }
}
