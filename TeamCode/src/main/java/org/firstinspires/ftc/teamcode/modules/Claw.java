package org.firstinspires.ftc.teamcode.modules;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.Servo;

@Config
public class Claw {
    public static double rClosedPosition = 0.28;
    public static double lClosedPosition = 0.2;
    public static double rOpenedPosition = 0.4;
    public static double lOpenedPosition = 0.34;
    public static double lMiddlePosition = 0.25;
    public static double rMiddlePosition = 0.65;
    public static boolean isOpened;
    private final Servo left;
    private final Servo right;
    private FtcDashboard dashmetry;


    public Claw(LinearOpMode _opMode) {
        left = _opMode.hardwareMap.servo.get("left");
        right = _opMode.hardwareMap.servo.get("right");
        //open();
    }

    /** переменные со значением позиции клешней
     * левая клешня: при уменьшении значения становиться ближе к закрытому положению
     *               при увелечении значения становиться ближе к открытому положению
     * правая клешня: при уменьшении значения становиться ближе к открытому положению
     *                при увелечении значения становиться ближе к закрытому положению
     * */

    /**
     * метод, который открывает клешню
     */
    public void open() {
        left.setPosition(lOpenedPosition);
        right.setPosition(rOpenedPosition);
        isOpened = true;
    }

    /**
     * метод, который закрывает клешню
     */
    public void close() {
        left.setPosition(lClosedPosition);
        right.setPosition(rClosedPosition);
        isOpened = false;
    }

    /**
     * метод, который расслабляет клешню
     */
    public void middle() {
        left.setPosition(lMiddlePosition);
        right.setPosition(rMiddlePosition);
    }

    /**
     * Этот метод говорит нам, открыта(true) или закрыта(false) наша клешня.
     */
    public boolean isOpened() {
        return isOpened;
    }

    public void dashClaw() {
        dashmetry.getTelemetry().addData("lclospos", lClosedPosition);
        dashmetry.getTelemetry().addData("lopenpos", rOpenedPosition);
        dashmetry.getTelemetry().addData("rclospos", rClosedPosition);
        dashmetry.getTelemetry().addData("ropenpos", rOpenedPosition);

    }
}
