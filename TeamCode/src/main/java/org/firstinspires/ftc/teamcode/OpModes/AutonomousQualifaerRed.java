package org.firstinspires.ftc.teamcode.OpModes;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.modules.Claw;
import org.firstinspires.ftc.teamcode.modules.Drivetrain;
import org.firstinspires.ftc.teamcode.modules.Elevator;
import org.firstinspires.ftc.teamcode.modules.recognition.Recognition;

@Autonomous(group = "Autonomous", name = "AutonomousQualifaerRed")
public class AutonomousQualifaerRed extends LinearOpMode {
    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Claw cl = new Claw(this);
        Recognition rc = new Recognition(this);
        Elevator el = new Elevator(this);
        cl.close();
        int id = 28;
        if (rc.getId() != -1) {
            id = rc.getId();
        }
        waitForStart();
        dt.encoder(5000, 0.7);
        dt.coursePID(135);
        el.drive(0.7);
        sleep(2500);
        el.drive(0);
        cl.open();
        dt.coursePID(0);
        dt.encoder(500, 0.5);
        if (id == 30) {
            dt.driveRight(0.7, 500);
        } else if (id == 32) {
            dt.driveLeft(0.7, 500);
        }
    }
}
