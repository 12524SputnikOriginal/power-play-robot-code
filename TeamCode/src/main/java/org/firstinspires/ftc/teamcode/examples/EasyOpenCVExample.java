package org.firstinspires.ftc.teamcode.examples;

import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.modules.recognition.Recognition;
import org.openftc.apriltag.AprilTagDetection;

@TeleOp(name = "EasyOpenCVExample")
@Config
public class EasyOpenCVExample extends LinearOpMode {

    AprilTagDetection tagOfInterest = null;

    @Override
    public void runOpMode() {

        Recognition reco = new Recognition(this);

        while (!isStarted() && !isStopRequested()) {
            telemetry.addData("id:", reco.getId());
            telemetry.update();
        }
    }
}