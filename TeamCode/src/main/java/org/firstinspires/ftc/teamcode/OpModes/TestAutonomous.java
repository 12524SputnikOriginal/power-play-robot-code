package org.firstinspires.ftc.teamcode.OpModes;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.modules.Claw;
import org.firstinspires.ftc.teamcode.modules.Drivetrain;
import org.firstinspires.ftc.teamcode.modules.recognition.Recognition;

@Autonomous(group = "Autonomous", name = "TestAuto")
public class TestAutonomous extends LinearOpMode {
    public static double DRIVE_POWER = 0.6;
    public static double TICK = 1300;

    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Claw cl = new Claw(this);
        Recognition rc = new Recognition(this);
        cl.close();
        while (!isStarted()) {
            telemetry.addData("id", rc.getId());
            telemetry.update();
        }
        int id = 32;
        if (rc.getId() != -1)
            id = rc.getId();
        waitForStart();
        telemetry.addData("id", rc.getId());
        telemetry.update();
        if (rc.getId() != -1
        )
            id = rc.getId();
        dt.encoder(TICK, DRIVE_POWER);
        dt.encoder(100, -0.5);
        if (id == Recognition.FIRST_ZONE_CODE) {
            dt.coursePID(270);
            dt.encoder(750, -DRIVE_POWER);
            dt.stop();
        } else if (id == Recognition.THIRD_ZONE_CODE) {
            dt.coursePID(90);
            dt.encoder(750, -DRIVE_POWER);
            dt.stop();
        }
    }
}
