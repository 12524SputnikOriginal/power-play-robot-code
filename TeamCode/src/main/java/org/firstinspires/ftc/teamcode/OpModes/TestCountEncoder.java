package org.firstinspires.ftc.teamcode.OpModes;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.modules.Drivetrain;

//@Autonomous(group = "Red", name="")
@TeleOp(group = "Test", name = "CountEncoder")
@Disabled
public class TestCountEncoder extends LinearOpMode {
    //
    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        // единожды исполняемые действия перед инициализацией
//        while (opModeInInit()){ // многократно исполняемые действия после инициализации
//
//        }
        // единожды исполняемые действия после инициализацией
        waitForStart();
        // единожды исполняемые действия после старта
        while (opModeIsActive()) { // единожды исполняемые действия после старта
            dt.addDashAndTelePositions();

        }
    }
}
