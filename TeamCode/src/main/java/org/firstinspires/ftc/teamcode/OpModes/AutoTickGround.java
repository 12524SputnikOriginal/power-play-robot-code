package org.firstinspires.ftc.teamcode.OpModes;

import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.modules.Claw;
import org.firstinspires.ftc.teamcode.modules.Drivetrain;
import org.firstinspires.ftc.teamcode.modules.ImuSensor;


@Config
@Autonomous(group = "Autonomous", name = "AutoTickGround")
public class AutoTickGround extends LinearOpMode {
    public static double DRIVE_POWER = -0.5;
    public static double TICK_PARK = 1120;
    public static double TICK_GROUND = 490;
    public static double TICK_BACK = 1120;
    public static double ANGLE_GROUND = 45;
    public static double TICK_TO_GROUND = 20;

    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        ImuSensor imu = new ImuSensor(this);
        Claw cl = new Claw(this);
        cl.middle();
//        telemetry.addData("Angle", imu.getAngles());
//        telemetry.addData("DRIVE POWER:", DRIVE_POWER);
//        telemetry.update();
//        dashmetry.addData("Angle", imu.getAngles());
//        dashmetry.addData("DRIVE POWER:", DRIVE_POWER);
//        dashmetry.update();
        waitForStart();
//        telemetry.addData("Angle", imu.getAngles());
//        telemetry.addData("DRIVE POWER:", DRIVE_POWER);
//        dt.coursePID(ANGLE_GROUND);
//        dt.encoder(TICK_TO_GROUND, DRIVE_POWER);
//        dt.coursePID(0);
        dt.encoder(TICK_GROUND, DRIVE_POWER);
        cl.open();
        sleep(500);
        dt.encoder(TICK_BACK, -DRIVE_POWER);
        sleep(250);
        dt.encoder(TICK_PARK, DRIVE_POWER);
        sleep(250);


    }
}