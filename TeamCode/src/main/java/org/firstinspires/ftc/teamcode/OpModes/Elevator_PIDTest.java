package org.firstinspires.ftc.teamcode.OpModes;

 import com.acmerobotics.dashboard.config.Config;
        import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
        import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

        import org.firstinspires.ftc.teamcode.modules.Elevator;
@TeleOp
@Config

public class Elevator_PIDTest extends LinearOpMode {
    @Override
    public void runOpMode() {
        Elevator El = new Elevator(this);
        waitForStart();
      if(gamepad2.dpad_right){
          El.setTargetPosition(100);
      }
    }
}

