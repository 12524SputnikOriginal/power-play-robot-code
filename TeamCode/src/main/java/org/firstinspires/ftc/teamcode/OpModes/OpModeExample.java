package org.firstinspires.ftc.teamcode.OpModes;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

//@Autonomous(group = "Red", name="")
@TeleOp(group = "Test", name = "Example")
@Disabled
public class OpModeExample extends LinearOpMode {
    //
    @Override
    public void runOpMode() {
        // единожды исполняемые действия перед инициализацией
        while (opModeInInit()) { // многократно исполняемые действия после инициализации

        }
        // единожды исполняемые действия после инициализацией
        waitForStart();
        // единожды исполняемые действия после старта
        while (opModeIsActive()) { // единожды исполняемые действия после старта


        }
    }
}
